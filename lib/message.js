'use strict';

class Message {
  constructor() {
    this.connection = null;
    this.status = 200;
    this.headers = {};
    this.body = {};
  }

  getConnection() {
    return this.connection;
  }

  setConnection(connection) {
    this.connection = connection;
    return this;
  }

  getStatus() {
    return this.status;
  }

  setStatus(status) {
    this.status = status;
    return this;
  }

  getHeaders() {
    return this.headers;
  }

  setHeaders(headers) {
    this.headers = headers;
    return this;
  }

  getHeader(name) {
    return this.headers[name];
  }

  setHeader(name, value) {
    this.headers[name] = value;
    return this;
  }

  getBody() {
    return this.body;
  }

  setBody(body) {
    this.body = body;
    return this;
  }

  get(name) {
    return this.body[name];
  }

  set(name, value) {
    this.body[name] = value;
    return this;
  }

  delete(name) {
    delete this.body[name];
    return this;
  }

  toJSON() {
    return {
      body: this.body,
      headers: this.headers,
      status: this.status
    };
  }

  clone() {
    const headers = Object.assign({}, this.headers);
    const body = Object.assign({}, this.body);

    return new Message()
      .setConnection(this.connection)
      .setStatus(this.status)
      .setHeaders(headers)
      .setBody(body);
  }
}

module.exports = Message;
